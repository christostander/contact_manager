<h1>Contacts</h1>

<div class="navicationBtnContainer">
	<a href="<?php echo Yii::app()->createUrl('contacts/create'); ?>" class="btn btn-primary">Create a Contact</a>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'contacts-grid',
	'dataProvider'=>$model->search(),
	'pager' => array('cssFile' => Yii::app()->request->baseUrl.'/css/pager.css'),
	'cssFile' => Yii::app()->request->baseUrl.'/css/styles.css',
	'filter'=>$model,
	'columns'=>array(
		'id',
		'firstname',
		'lastname',
		'cellphone_number',
		'contact_type_id',
		'created_date',
		/*
		'updated_date',
		'status_id',
		*/
		array(
			'header'             => 'Action',
			'class'=>'CButtonColumn',
			'template' => '{update}{delete}',
			'deleteConfirmation' => 'Are you sure you want to delete this item?',
			'buttons'            => array(
                'update' => array(
                    'imageUrl' => false,
                    'url'      => 'Yii::app()->createUrl("contacts/update", array("id"=>$data->id))',
                    'label'    => '<i class="fa fa-pencil fa-fw"></i>',
                    'options' => array('data-toggle' => 'tooltip', 'title' => 'update'),
				),
				'delete' => array(
                    'imageUrl' => false,
                    'label'    => '<i class="fa fa-times-circle fa-fw"></i>',
                    'options' => array('data-toggle' => 'tooltip', 'title' => 'disable','data-placement' => 'left'),
                ),
			),
		),
	),
)); ?>
