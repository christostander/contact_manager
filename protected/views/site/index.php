<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<h1>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>
<h3>Instructions:</h3>
<p>
<ul>
    <li>Create a mySQL database with name contact_manager_your_name (where your_name is your firstname). Remember to change the Yii config accordingly to connect to this database.</li>
    <li>Create a table called 'contacts' with the following columns:
        <ul>
            <li>id int auto increment primary key not null</li>
            <li>firstname varchar not null</li> 
            <li>lastname varchar not null</li>
            <li>cellphone_number varchar of lenth 10 not null</li>
            <li>contact_type_id int not null</li>
            <li>created_date datetime null</li>
            <li>updated_date datetime null</li>
            <li>status_id int not null default 1</li>
        </ul>
    </li>
    <li>Create a table called 'contact_types' with the following columns:
        <ul>
            <li>id int auto increment primary key not null</li>
            <li>description varchar not null</li>
            <li>status_id int not null default 1</li>
        </ul>
    </li>
    <li>In the 'contact_types' table add two rows one called 'Family' and the other one called 'Friends'</li>
    <li>Remove the ID & Created Date Column for the Grid View</li>
    <li>When creating/updating a contact, remove the Created Date & Updated Date fields</li>
    <li>When creating/updating a contact, change the Contact Type field to be a drop down (this field should get it's data from the contact_types table). Also add a "-- Please Select --" to the drop down so that it has to be selected</li>
    <li>When creating/updating a contact, change the Status field to be a drop down. (This field can have hard coded values of 0 or 1). Also add a "-- Please Select --" to the drop down so that it has to be selected</li>
    <li>When updating a contact, change the heading to read "Updating Contact {firstname lastname}" where firstname and lastname is the contact's firstname and lastname that you are updating</li>
    <li>All the fields on the form should make use of the form-control bootstrap css class</li>
    <li>Add the form-control and primary btn bootstrap css classes to the Create/Save save button on the form</li>
    <li>Add a "Back to Contacts" primary bootstrap css class button to the form</li>
    <li>When creating/updating a contact, add a flash message status if the record was successfully created/updated</li>
    <li>Make sure that after creating/updating a contact, that the page redirects to the admin page</li>
    <li>When creating/updating a contact, make sure that the created_date and updated_date gets populated accordingly on the 'contacts' table in the database</li>
    <li>Change the Contact Type column to read from the 'contact_types' table and display the description</li>
    <li>Change the values of the Contact Type column to be a link. When clicking on the link it should do an ajax request to display all the contacts (firstname, lastname and cellphone_number) in alphabetical order that are of that type in a jQuery UI dialog</li>
    <li>Add the Status column with a drop down filter to the Grid View, after the Contact Type field</li>
    
</ul>
<h3>BONUS</h3>
<p>----------------------------------------------------------------------------</p>
<ul>
    <li>Change the ordering of the Grid View to sort by firstname ASC, lastname ASC</li>
    <li>When disabling a contact, please change the status_id in the contacts table accordingly. Active = 1 and Inactive = 0</li>
<ul>
</p>
